---
home: true
navbar: false
sidebar: true
heroImage: /logo.png
heroText: Smart Blog
tagline: 賢く書いていくためのブログ作成ツール
actionText: 覗いてみる →
actionLink: /developer
features:
- title: ずっと無料で使える
  details: ブログの作成にお金が掛かると思っていませんか？Smart Blogなら作成から公開まで無料で行えます。
- title: 強力なブログ作成支援機能
  details: 便利なマークダウン記法に加え、ボタンや広告など、必要なパーツは全てドラッグ&ドロップで設置頂けます。
- title: ブログ初心者にお勧め
  details: 試しに書いてみたい！そんな人に対してSmart Blogは費用・学習におけるコストを極限まで減らしてくれます。
footer: Copyright © 2020-present Smart Blog
---