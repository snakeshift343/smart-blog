module.exports = {
  dest: 'vuepress',
  serviceWorker: true,
  plugins: [
    "@vuepress/blog",
  ],
  themeConfig: {
    logo: '/logo.png',
    sidebar: [
      [
        '/developer',
        'ブログを書く'
      ],
      {
        title: 'Components',   // required
        path: '/foo/',      // optional, which should be a absolute path.
        collapsable: false, // optional, defaults to true
        sidebarDepth: 1,    // optional, defaults to 1
        children: [
          '/page1'
        ]
      }
    ]
  },
  title: 'Smart Blog',
  base: '/'
}